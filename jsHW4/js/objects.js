"use strict";
function isCheckName(name) {
    while ((!name)||(name == null)||(!isNaN(name))) name = prompt('Enter your name:', name);
    return name;
}
function createNewUser() {
    let newUser = {
        firstName: isCheckName(prompt('Enter your first name:')),
        lastName: isCheckName(prompt('Enter your last name:')),
        getLogin: function(){
           return (this.firstName.charAt(0) + this.lastName).toLowerCase();
        },
        setFirstName: function(value) {
            Object.defineProperty(this, 'firstName', {
                writable: true
            });
            this.firstName = value;
            Object.defineProperty(this, 'firstName', {
                writable: false
            });
        },
        setLastName: function(value) {
            Object.defineProperty(this, 'lastName', {
                writable: true
            });
            this.lastName = value;
            Object.defineProperty(this, 'lastName', {
                writable: false
            });
        }
    };
    Object.defineProperties(newUser, {
        'firstName': {
            writable: false
        },
        'lastName': {
            writable: false
        }
    });
    return newUser;
}
let user = new createNewUser();
console.log(user.getLogin());
console.log(user);
user.setFirstName('Sanya');
console.log(user);
user.firstName = 'Vasya';
console.log(user);